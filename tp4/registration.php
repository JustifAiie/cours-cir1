<?php
  
  session_start();

  try{
        $bdd = new PDO('mysql:host=localhost;dbname=Calendrier;charset=utf8','root', 'isencir');
    }catch(Exception $e){
        exit("Erreur : 0" .$e -> getMessage());
    }

    $username = htmlentities($_POST['username'], ENT_QUOTES, "ISO-8859-1");  
    $password = htmlentities($_POST['password'], ENT_QUOTES, "ISO-8859-1");
    $hashed = password_hash($password, PASSWORD_DEFAULT);
    $rank = htmlentities($_POST['rank'], ENT_QUOTES, "ISO-8859-1");

  $check = $bdd->prepare('INSERT INTO Users (login, password, rank) VALUES (?, ?, ?)');
  $check->execute(array($username, $hashed, $rank)); 

  header("Location: ../index.php");
?>