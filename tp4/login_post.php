<form  class="cf" method="post" action="session.php" enctype="multipart/form-data">
      
      <h1> CONNEXION </h1>
      <form>
        <input type="text" name="username" placeholder="Identifiant"/>
        <input type="password" name="password" placeholder="Mot de passe"/>
        <?php 
            session_start();
            if(!empty($_SESSION['error']))
            {
                echo "<div class=\"error\">";
                echo htmlspecialchars($_SESSION['error']);
                echo "</div>"; 
                $_SESSION['error'] = "";
            }
        ?>
        <input type="submit" name="submit" value=" Connexion " id="submit">
        <p class="message">Pas de compte ? <a href="register.php">Créer un compte</a></p>
      </form>

</form>
