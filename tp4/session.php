<?php
session_start();

if(empty($_POST['username'])) {
    echo "Identifiant vide !";
} 
else {    
    if(empty($_POST['password'])) {
        echo "Mot de passe vide !";
    } 
    else {  

        $username = htmlentities($_POST['username'], ENT_QUOTES, "ISO-8859-1");  
        $password = htmlentities($_POST['password'], ENT_QUOTES, "ISO-8859-1");

        try {
            $bdd = new PDO('mysql:host=localhost;dbname=Calendrier;charset=utf8','root', 'isencir');
        }
        catch(Exception $e) {
            exit("Erreur : 0" .$e -> getMessage());
        }

        $check = $bdd->prepare("SELECT Users.login, Users.password FROM Calendrier.Users WHERE login = :username");
        $check->execute(array(':username'=>$username));
        $data = $check->fetch();

        if(password_verify($password, $data['password']))
        {
            $_SESSION['username'] = $username;
            header("Location: ../Calendrier.php");
        }
        else
        {
            $_SESSION['error'] = "Identifiant ou mot de passe invalide !";
            header("Location: ../index.php");
        }
    }
}
?>