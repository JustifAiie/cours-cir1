<!DOCTYPE html>
<html>
    <head>
        <title>Kwen an bon</title>
        <meta charset="UTF-8">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
        <link rel="stylesheet" href="./style.css" type="text/css" />
    </head>
    <body>
        <h1>Kwen an bon</h1>
        <section id="form">
        	<form action="traitement.php" method="POST" enctype="multipart/form-data">
                <fieldset>
                    <p>
          			   <label for="product">Produit :</label>
          			   <input type="text" name="product" id="product" size="30" required />
                    </p>
                    <p>
          			   <label for="price">Prix :</label>
          			   <input type="number" step="0.01" name="price" id="price" required />
                    </p>
                    <p>
                        <label for="quantity">Quantité :</label>
                        <input type="number" name="quantity" id="quantity" min="0" required />
                    </p>
                    <p>
                        <label for="photo">Envoyer un fichier :</label>
                        <input type="file" name="photo" id="photo" required />
                    </p>
                    <p>
          		        <input type="submit" value="Mettre votre produit en vente" id="button" />
                    </p>
                </fieldset>
        	</form>
        </section>
    </body>
</html>

