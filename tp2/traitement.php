<?php


  	/*$name = htmlspecialchars($_POST['product'], ENT_QUOTES);
    $price = htmlspecialchars(round($_POST['price'], 2), ENT_QUOTES);
    $quantity = htmlspecialchars($_POST['quantity'], ENT_QUOTES);*/

    $name = $_POST['product'];
    $price = $_POST['price'];
    $quantity = $POST['quantity'];

    $valid_ext = array('jpg', 'jpeg', 'png');
    $upload_ext = strtolower(substr(strrchr($_FILES['photo']['name'], '.'), 1));
    $maxsize = 1000000;
    /*$maxwidth = 800;
    $maxheight = 800;*/


    if (!in_array($upload_ext, $valid_ext)) {
    	die('Une image doit être envoyée dans un format adapté ! (jpg, jpeg, png)');
    }

    if ($_FILES['photo']['size'] > $maxsize) {
    	die('La photo de votre produit est trop lourde !');
    }

    /*$photo_size = getimagesize($_FILES['photo']['tmp_name']);
    if ($photo_size[0] > maxwidth OR $photo_size[1] > maxheight) {
    	die('Les dimensions de votre photo sont trop grandes !');
    }*/

   	/*if(preg_match("#^[a-z0-9A-Z ]+$#",$name) == FALSE) {
    	die('Le nom de votre produit doit contenir uniquement des lettres et des chiffres !');
    }*/
    if ($price < 0) {
    	die('Le prix de votre porduit doit être positif !');
    }

    $item = array($name, $price, $quantity);

    $class = fopen("mesproduits.csv", "a+");
    fwrite($class, $name.";");
    fwrite($class, $price.";");
    fwrite($class, $quantity.";");


    header('Location: index.php');
    exit();

?>