<!DOCTYPE html>
<html>
	<head>
		<title>Le GIGA chat</title>
		<meta charset="UTF-8">
	</head>

	<body>
		<form action="chat_post.php" method="POST">
			<p>
          		<label for="Nickname">Pseudo : </label>
          		<input type="text" name="nickname" id="nickname" size="100" required />
            </p>
            <p>
          		<label for="Message">Message : </label>
          		<input type="text" name="message" id="message" required />
            </p>
            <p>
            	<input type="submit" value="Envoyer">
            </p>
        </form>

        <?php

        try {
          $bdd = new PDO('mysql:host=localhost;dbname=Chat;charset=utf8', 'root', 'isencir');
        }
        catch (Exception $e) {
          die('Erreur : 0'.$e->getMessage());
        }

        $answer = $bdd->query('SELECT nickname, message FROM messages ORDER BY ID DESC LIMIT 0, 10');

        while ($data = $answer->fetch()) {
          echo '<p>' . htmlspecialchars($data['nickname']) . htmlspecialchars($data['message']) . '<p>';
        }

        $answer->closeCursor();

        ?>

    </body>
</html>

